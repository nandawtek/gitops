process.env.NODE_ENV = 'testing'
const request = require('supertest')
const app = require('../index')

describe('System', () => {
  test('say hello', async () => {
    const response = await request(app).get(`/`)

    expect(response.text).toBe('Hello')
  })
})
